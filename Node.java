/*
 * Node java file contain a constructor class of tree Node.
 * This class contains three data members and along with a parameterized constructor
 */

public class Node{
	int val;				// variable to store node value
	Node left;				// left pointer of type Node
	Node right;				// right pointer of type Node
	public Node(int val) {
		this.val = val;			// store value in class variable
		left = right = null;	// assign null to the left and right pointers
	}
}