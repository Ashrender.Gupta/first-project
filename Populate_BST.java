/*
 * This java file contain the main function where code starts its execution
 */

import java.io.IOException;
import java.util.*;

public class Populate_BST {
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BST tree = new BST();				// created object of BST class
		@SuppressWarnings("resource")       // <---- Auto suggestion line for Scanner object "s"
		Scanner s = new Scanner(System.in);	// created object of scanner class used for user input
		Random random = new Random();
		System.out.print("Enter max node range value: ");
		int max = s.nextInt();
		
		while(true) { // loop, till condition the true
			
			System.out.print("Enter: 1 -> Add node or nodes, 2 -> display inorder of BST, 3 -> Display level of BST, 4 -> exit: ");
			int c = s.nextInt();	// integer input for switch case loop
			switch(c) {
			case 1:
				System.out.print("Enter no. of node to be added in BST: ");
				int n = s.nextInt();
				
				for(int i = 0; i < n; i++) {	// loop to add node to BST
					int val = random.nextInt(max);
					tree.insert_node(val);
				}
				break;
			case 2:
				tree.inorder_traversal();	// for in-order traversal of BST
				System.out.println();
				break;
			case 3:
				tree.BFS_Traversal();		// for BSF traversal of BST
				break;
			case 4:
				System.out.print("Program Over!!!");
				return;
			}
		}
	}
}






//public class Populate_BST {
//	public static void main(String[] args) throws IOException {
//		// TODO Auto-generated method stub
//		BST tree = new BST();				// created object of BST class
//		@SuppressWarnings("resource")       // <---- Auto suggestion line for Scanner object "s"
//		Scanner s = new Scanner(System.in);	// created object of scanner class used for user input
//		
//		while(true) { // loop, till condition the true
//			
//			System.out.print("Enter: 1 -> Add node or nodes, 2 -> display inorder of BST, 3 -> Display level of BST, 4 -> exit: ");
//			int c = s.nextInt();	// integer input for switch case loop
//			switch(c) {
//			case 1:
//				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); // for steam input
//				System.out.print("Enter value/values: ");
//				String[] inp_str = reader.readLine().split(" ");
//				
//				for(int i = 0; i < inp_str.length; i++) {	// loop to add node to BST
//					tree.insert_node(Integer.parseInt(inp_str[i]));
//				}
//				break;
//			case 2:
//				tree.inorder_traversal();	// for in-order traversal of BST
//				System.out.println();
//				break;
//			case 3:
//				tree.BFS_Traversal();		// for BSF traversal of BST
//				break;
//			case 4:
//				System.out.print("Program Over!!!");
//				return;
//			}
//		}
//	}
//}






//class BST_Recursive{
//	Node root;
//	BST_Recursive(){
//		root = null;
//	}
//	void insert_node(int val) {
//		root = insert(root, val);
//	}
//	
//	Node insert(Node node, int val) {
//		if(node == null) {
//			node = new Node(val);
//			return node;
//		}
//		if(val < node.val) {
//			node.left = insert(node.left, val);
//		}
//		else if(val > node.val){
//			node.right = insert(node.right, val);
//		}
//		return node;
//	}
//	void inorder_traversal() {
//		inorder(root);
//	}
//	
//	void inorder(Node node) {
//		if(node != null) {
//			inorder(node.left);
//			System.out.print(node.val + " ");
//			inorder(node.right);
//		}
//	}
//}
//
//public class Populate_BST {
//
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		BST_Recursive tree = new BST_Recursive();
//		while(true) {
//			System.out.print("Enter: 1 -> Add node, 2 -> display inorder of BST, 3 -> exit: ");
//			Scanner s = new Scanner(System.in);
//			int c = s.nextInt();
//			switch(c) {
//			case 1:
//				System.out.print("Enter node value: ");
//				int key = s.nextInt();
//				tree.insert_node(key);
//				break;
//			case 2:
//				tree.inorder_traversal();
//				System.out.println();
//				break;
//			case 3:
//				System.out.print("Program Over!!!");
//				return;
//			}
//			
//		}
//	}
//
//}