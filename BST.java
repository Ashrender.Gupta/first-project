/*
 * This java file contain a class constructor with name of BST (binary search tree).
 * This class contains some operations related to Binary Search Tree and also uses the Node class.
 */

import java.util.LinkedList;
import java.util.Queue;

class BST{
	Node root;						// root node declaration
	BST(){							// default constructor to initialize the root node as null
		root = null;
	}
	
	// function to add/insert new node in BST tree
	void insert_node(int val) {
		Node node = new Node(val);	// new node created
		if(root == null) {			// adding first node in BST tree
			root = node;
		}
		else {
			Node curr_ptr = root;		// current node pointer pointing to root node
			Node prev_ptr = null;		// previous node pointer used to pointing parent node of curr_node
			while(curr_ptr != null) {	// loop
				prev_ptr = curr_ptr;
				if(curr_ptr.val > val) {		// condition if curr_ptr value greater than insert_value
					curr_ptr = curr_ptr.left;	// curr_ptr point to its left child node
				}
				else if(curr_ptr.val < val) {	// condition if curr_ptr value less than insert_value
					curr_ptr = curr_ptr.right;	// curr_pte point to its right child node
				}
				else {			// if curr_ptr value equals to the insert_value than no need to insert new node
					break;
				}
			}
			if(prev_ptr != null) {				// condition to insert new node
				if(prev_ptr.val > val) {		// condition to add new node to the left of prev_ptr node
					prev_ptr.left = node;
				}
				else if(prev_ptr.val < val) {	// condition to add new node to the right of prev_ptr node
					prev_ptr.right = node;
				}
			}
		}
	}
	void inorder_traversal() {					// in-order traversal of BST
		inorder(root);
	}
	
	void inorder(Node node) {			// recursive function of in-order traversal of BST
		if(node != null) {				// condition if current node is not null
			inorder(node.left);
			System.out.print(node.val + " ");
			inorder(node.right);
		}
	}
	
	void BFS_Traversal() {						// BST traversal by Breadth First Search or level order
		Queue<Node> q = new LinkedList<>();		// initialized queue
		q.add(root);
		while(!q.isEmpty()) {					// loop till queue not empty
			int count = q.size();
			
			while(count > 0) {					// nested loop till value of count is positive
				Node curr_node = q.poll();
				System.out.print(curr_node.val + " ");
				if(curr_node.left != null) {			// condition if curr_node have left node
					q.add(curr_node.left);
				}
				if(curr_node.right != null) {			// condition if curr_node have right node
					q.add(curr_node.right);
				}
				count -= 1;
			}
			System.out.println();
		}
		
	}
}